package sample

import "fmt"


// use the local key binding to test faster:
// (local-set-key (kbd "C-M-g") 'gtn/open-navigator)

func TestTeacherResolver_Part1(t *testing.T) {

	 tests := struct {
	 	   name string
		   args string
		   want string
		   wantErr *Error
	 }{
		{
			name: "P1 ok 1",
			args: "aiueo",
			want: "whatever",
			wantErr: nil,
		},
		{
			name: "P1 ng 1",
			args: "1234",
			want: "",
			wantErr: errors.New("you f*ed up"),
		},
		{
			name: "P1 ng 2",
			args: "5678",
			want: "",
			wantErr: errors.New("I f*ed up"),
		},
	 }

	 // ... run loop ...
}


func TestTeacherResolver_other3(t *testing.T) {

	 tests := struct {
	 	   name string
		   args string
		   want string
		   wantErr *Error
	 }{
		{
			name: "P2 test ok 1",
			args: "aiueo",
			want: "whatever",
			wantErr: nil,
		},
		{
			name: "P2 test ng 1",
			args: "1234",
			want: "",
			wantErr: errors.New("you f*ed up"),
		},
		{
			name: "P2 test ng 2",
			args: "5678",
			want: "",
			wantErr: errors.New("I f*ed up"),
		},
	 }

	 // ... run loop ...
}
