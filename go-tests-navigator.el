
(make-variable-buffer-local
  (defvar go-tests-navi-mode nil
    "Toggle go-tests-navi-mode."))

(defvar go-tests-navi-mode-map (make-sparse-keymap)
  "The keymap for go-tests-navi-mode")

;; Define a key in the keymap
(define-key go-tests-navi-mode-map (kbd "<f5>")
  (lambda ()
    (interactive)
    (gtn/build-navigator)))

(define-key go-tests-navi-mode-map (kbd "<next>")
  (lambda ()
    (interactive)
	(let ((currentspot (point))
		  (found1 nil)
		  (found2 nil))
	  ;; search first
	  (re-search-forward gtn/-re-subtest nil t 1)
	  (setq found1 (max currentspot (point)))
	  ;; back to previous spot
	  (goto-char currentspot)
	  ;; search second
	  (re-search-forward gtn/-re-subtest2 nil t 1)
	  (setq found2 (max currentspot (point)))
	  (message "found spots: %d %d" found1 found2)
	  ;; go to closest spot
	  (goto-char (max found1 found2))
	  (recenter)
      (gtn/build-navigator))))

(define-key go-tests-navi-mode-map (kbd "<prior>")
  (lambda ()
    (interactive)
	(let ((currentspot (point))
		  (found1 nil)
		  (found2 nil))
	  ;; search first
	  (re-search-backward gtn/-re-subtest nil t 2)
	  (re-search-forward gtn/-re-subtest nil t 1)
	  (setq found1 (min currentspot (point)))
	  ;; search second
	  (re-search-backward gtn/-re-subtest2 nil t 2)
	  (re-search-forward gtn/-re-subtest2 nil t 1)
	  (setq found2 (min currentspot (point)))
	  ;; go to closest position
	  (goto-char (min found1 found2))
	  (recenter)
      (gtn/build-navigator))))

(add-to-list 'minor-mode-alist '(go-tests-navi-mode " go-tests-navi"))
(add-to-list 'minor-mode-map-alist (cons 'go-tests-navi-mode go-tests-navi-mode-map))

(defun go-tests-navi-mode (&optional ARG)
  (interactive (list 'toggle))
  (setq go-tests-navi-mode
        (if (eq ARG 'toggle)
            (not go-tests-navi-mode)
          (> ARG 0)))

  ;; Take some action when enabled or disabled
  (if go-tests-navi-mode
      (progn
		(gtn/setup)
		(message "go-tests-navi-mode activated!"))
    (message "go-tests-navi-mode deactivated!")))


;;; --------------------------------------------------

(defconst gtn/-re-maintest "^func Test\\(.*\\)(" "Regex to find main test")
(defconst gtn/-re-subtest "{\n.*name:.* \"\\([^\"]+\\)\"" "Regex to find sub test name:...")
(defconst gtn/-re-subtest2 "t\.Run(\"\\([^\"]+\\)\"," "Regex to find sub test t.Run...")

(defvar gtn/tests-nav-buffername "*go-tests-navigator*")
(defvar gtn/tests-nav-point-indicator ">------------------<")

(defun gtn/-find-sub-tests ()
  "Searched for sub-tests"
  (let ((found (list)))
	;; first pattern
	(beginning-of-buffer) 
	(while (re-search-forward gtn/-re-subtest nil t)
	  (setq found (cons
				   (cons (point) (format "  - %s" (match-string-no-properties 1)))
				   found)))
	;; second pattern
	(beginning-of-buffer) 
	(while (re-search-forward gtn/-re-subtest2 nil t)
	  (setq found (cons
				   (cons (point) (format "  - %s" (match-string-no-properties 1)))
				   found)))
	found))

(defun gtn/-find-main-tests ()
  "To be called to start parsing the while file matching Test..."
  (let ((found (list)))
	(beginning-of-buffer)
	(while (re-search-forward gtn/-re-maintest nil t)
	  (point-at-bol)
	  ;; save text and position + sub-tests if any
	  (setq found (cons
				   (cons (point) (format "* %s" (match-string-no-properties 1)))
				   found)))
	found))

(defun gtn/-funcs-sorter (a b) (< (car a) (car b)))

(defun gtn/-find-all ()
  (let* ((main-funcs (gtn/-find-main-tests))
		 (sub-funcs  (gtn/-find-sub-tests))
		 (all-entries (append main-funcs sub-funcs))
		 (sorted-entries (sort all-entries 'gtn/-funcs-sorter)))
	sorted-entries))

(defun gtn/-refresh-navigator ()
  "Recenters the window where the tests-navigator buffer is."
  (save-excursion
	(with-selected-window (get-buffer-window gtn/tests-nav-buffername)
	  (beginning-of-buffer)
	  (search-forward gtn/tests-nav-point-indicator nil t)
	  (recenter))
	))

(defun gtn/build-navigator ()
  "Main function to generate the map of the file."
  (interactive)
  (save-excursion
	(save-window-excursion
	  (let* ((current-point (point))
			 (sorted-entries (gtn/-find-all))
			 (index 0)
			 (found (nth index sorted-entries))
			 (mark-shown nil))
		;; switch to different buffer after looking into the go tests file
		(switch-to-buffer-other-window gtn/tests-nav-buffername)
		;; clear navigator buffer
		(erase-buffer)
		;; add our entries
		(while found
		  (when (and (not mark-shown)
					 (> (car found) current-point))
			(insert gtn/tests-nav-point-indicator "\n")
			(setq mark-shown t))
		  (insert (cdr found) "\n")
		  (setq index (+ 1 index))
		  (setq found (nth index sorted-entries)))
		;; bottom of the list
		(when (not mark-shown)
		  (insert gtn/tests-nav-point-indicator "\n"))
		)))
  (gtn/-refresh-navigator)
  )

(defun gtn/-create-side-window ()
  (split-window-right)
  (unless (get-buffer gtn/tests-nav-buffername)
	(generate-new-buffer gtn/tests-nav-buffername))
  (display-buffer gtn/tests-nav-buffername '(display-buffer-pop-up-window . nil))
  )

(defun gtn/setup ()
  "setup for the first time loading a buffer"
  (interactive)
  (auto-revert-mode 1)
  ;; create the window if missing
  (gtn/-create-side-window)
  ;; call main function for the first time only
  (gtn/build-navigator)
  )
