# Disclaimer

My first *real* package, it's certainly badly done, but it helps me in my tasks, and that's all that matters for the moment.  
Though I would gladly accept advice to improve.

# Usage

Load the file in your main config for example:  
`(load "path/to/go-tests-navigator.el")`

Then you can activate the minor mode in a go buffer:  
`M-x go-tests-navi-mode`  

**Note:** the package will open a new window on the right no matter what (for now).

Use `Page UP` and `Page DOWN` to navigate between the tests.

# Supported files/tests

See the provided sample go file. Some other patterns might be supported in the future.
